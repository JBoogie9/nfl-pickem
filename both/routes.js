"use strict";

Router.route('/', {
    template: 'home'
});
Router.route('/week1', {
    template: 'week1'
});
Router.route('/week1results', {
    template: 'week1results'
});
Router.route('/profile', {
    template: 'profile'
});
